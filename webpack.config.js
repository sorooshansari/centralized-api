/* eslint-disable @typescript-eslint/no-var-requires */
const webpack = require('webpack');
const path = require('path');
const nodeExternals = require('webpack-node-externals');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const Dotenv = require('dotenv-webpack');
const CopyPlugin = require('copy-webpack-plugin');

const isProduction = typeof process.env.NODE_ENV !== 'undefined' && process.env.NODE_ENV === 'production';
const mode = isProduction ? 'production' : 'development';
const devtool = isProduction ? false : 'inline-source-map';

module.exports = {
    entry: './src/main.ts',
    optimization: {
        minimize: false,
    },
    target: 'node',
    mode,
    devtool,
    externals: [
        nodeExternals(),
    ],
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: 'ts-loader',
                options: {
                    transpileOnly: true
                },
                exclude: /node_modules/,
            }
        ]
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js'],
    },
    plugins: [
        new ForkTsCheckerWebpackPlugin(),
        new Dotenv({
            path: './.env', // Path to .env file (this is the default)
            safe: true // load .env.example (defaults to "false" which does not use dotenv-safe)
        }),
        new CopyPlugin({
          patterns: [
            { from: 'views', to: 'views' },
          ],
        }),
    ],
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'server.js',
    },
};