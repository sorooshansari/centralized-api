
## Description

Sample Api repository with NestJs.

## Installation

```bash
$ yarn install
```

## Running the app

```bash
# development
$ yarn start:dev
```
