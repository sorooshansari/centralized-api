import { ExecutionContext, Injectable, CanActivate } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { IAuthUser } from './authUser.model';
import { UsersService } from '../services/users/users.service';
import { Role } from '../models/entities/role.entity';
import { Permission } from '../models/enums/permission.enum';
import { UserType } from '../models/enums/user-type.enum';

@Injectable()
export class PermissionGuard implements CanActivate {
  constructor(
    private readonly reflector: Reflector,
    private readonly usersService: UsersService,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const user: IAuthUser = request.user;

    const userEntity = await this.usersService.findOneWithRelations(user.id, [
      'roles',
    ]);
    if (!userEntity) {
      return false;
    }
    request.userEntity = userEntity;

    const permission = this.reflector.get<Permission>(
      'permission',
      context.getHandler(),
    );
    if (!permission || userEntity.type === UserType.SUPER_ADMIN) {
      return true;
    }
    const userPermissions = [
      ...new Set(
        userEntity.roles
          .map(item => item.permissions.split(','))
          .reduce((accumulator, value) => accumulator.concat(value), []),
      ),
    ];
    return userPermissions.indexOf(permission.toString()) > -1;
  }
}
