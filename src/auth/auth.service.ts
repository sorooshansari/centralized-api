import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { createHash } from 'crypto';
import { configService } from '../config/config.service';
import { IAuthUser } from './authUser.model';

@Injectable()
export class AuthService {
  INTEGRITY_TOKEN = configService.getAuthIntegrityToken();
  constructor(private jwtService: JwtService) {}
  verify(token: string): IAuthUser {
    const authUser = this.jwtService.verify(token);
    if (this.validateUser(authUser)) {
      return authUser;
    }
    return null;
  }
  async createToken(user: IAuthUser): Promise<string> {
    user.integrity = this.createIntegrity(user);
    const token = this.jwtService.sign(user);
    return token;
  }

  async validateUser(signedUser: IAuthUser): Promise<boolean> {
    if (signedUser.integrity === this.createIntegrity(signedUser)) {
      return true;
    } else {
      return false;
    }
  }

  decode(token: string): IAuthUser {
    return this.jwtService.decode(token) as IAuthUser;
  }

  private createIntegrity(user: Partial<IAuthUser>) {
    const integrityString = `${user.id}%${user.type}%${this.INTEGRITY_TOKEN}%${user.email}`;
    return createHash('md5')
      .update(integrityString, 'utf8')
      .digest('hex');
  }
}
