import { Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthService } from './auth.service';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { IAuthUser } from './authUser.model';
import { configService } from '../config/config.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private authService: AuthService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: configService.getAuthSecretToken(),
    });
  }
  async validate(payload: IAuthUser): Promise<IAuthUser> {
    if (!(await this.authService.validateUser(payload))) {
      throw new UnauthorizedException();
    }
    return payload;
  }
}
