import { UserType } from "../models/enums/user-type.enum";

export interface IAuthUser {
  id: string;
  email: string;
  type: UserType;
  integrity?: string;
}
