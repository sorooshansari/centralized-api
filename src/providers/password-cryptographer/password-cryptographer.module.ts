import { Module } from '@nestjs/common';
import { PasswordCryptographerService } from './password-cryptographer';

@Module({
  providers: [PasswordCryptographerService],
  exports: [PasswordCryptographerService],
})
export class PasswordCryptographerServiceModule {}
