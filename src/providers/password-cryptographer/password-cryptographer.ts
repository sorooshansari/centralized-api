import * as bcrypt from 'bcrypt';
import * as crypto from 'crypto';
import { Injectable } from '@nestjs/common';
import { configService } from '../../config/config.service';

@Injectable()
export class PasswordCryptographerService {
  private encryptionInfo = configService.getEncryptionInfo();
  doHash(plaintextPassword: string): Promise<string> {
    return bcrypt.hash(plaintextPassword, this.saltRounds());
  }

  doCompare(plaintextPassword: string, hash: string): Promise<boolean> {
    return bcrypt.compare(plaintextPassword, hash);
  }

  private saltRounds() {
    return 5;
  }

  key = crypto.scryptSync(this.encryptionInfo.password, 'GfG', 24);
  iv = Buffer.alloc(16, 0);

  encrypt(text: string): string {
    const cipher = crypto.createCipheriv(
      this.encryptionInfo.algorithm,
      this.key,
      this.iv
    );
    let crypted = cipher.update(text, 'utf8', 'hex');
    crypted += cipher.final('hex');
    return crypted;
  }

  decrypt(text: string): string {
    const decipher = crypto.createDecipheriv(
      this.encryptionInfo.algorithm,
      this.key,
      this.iv
    );
    let dec = decipher.update(text, 'hex', 'utf8');
    dec += decipher.final('utf8');
    return dec;
  }
}
