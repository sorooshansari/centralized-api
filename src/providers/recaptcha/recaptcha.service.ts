import { Injectable, Logger } from '@nestjs/common';
import { ApiService } from '../api/api.service';
import { RecaptchaResponse } from './interface/recaptcha-response';

@Injectable()
export class RecaptchaService {
  constructor(private http: ApiService, private log: Logger) {}
  Verify(
    globalCaptchaResponse: string,
    remoteAddress: string,
  ): Promise<boolean> {
    // req.connection.remoteAddress will provide IP address of connected user.
    const endpoint = 'https://www.google.com/recaptcha/api/siteverify';
    const data = {
      // secret: ENV.RECAPTCHA_SECRET_KEY,
      response: globalCaptchaResponse,
      remoteip: remoteAddress,
    };
    return new Promise<boolean>((resolve, reject) => {
      this.http
        .get<RecaptchaResponse>({ endpoint, data })
        .then(res => {
          if (res.success) {
            resolve(true);
          } else {
            resolve(false);
          }
        })
        .catch(err => {
          this.log.error(
            err.message ? err.message : err,
            `${RecaptchaService.name}/Verify`,
            'Something went wrong with verify recaptcha ',
          );
          reject(err);
        });
    });
  }
}
