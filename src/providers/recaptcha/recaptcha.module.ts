import { Module } from '@nestjs/common';
import { RecaptchaService } from './recaptcha.service';
import { ApiModule } from '../api/api.module';

@Module({
  providers: [RecaptchaService],
  exports: [RecaptchaService],
  imports: [ApiModule],
})
export class RecaptchaServiceModule {}
