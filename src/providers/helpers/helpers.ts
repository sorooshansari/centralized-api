// import * as crypto from "crypto";
import { Injectable } from '@nestjs/common';

@Injectable()
export class Helpers {
  generateRandomNumber(): string {
    return Math.floor(10000 + Math.random() * 90000).toString();
  }
}
