import { Injectable } from '@nestjs/common';
import * as fs from 'fs';
import { compile } from 'handlebars';
import { createTransport, SendMailOptions } from 'nodemailer';
import * as path from 'path';
import { configService } from '../../config/config.service';

@Injectable()
export class EmailService {
  async sendMail({
    to,
    subject,
    html,
    text,
  }: {
    to: string;
    subject: string;
    html?: string;
    text?: string;
  }): Promise<any> {
    const SMTPOptions = configService.getSMTPOptions();
    const transporter = createTransport(SMTPOptions);
    const mailOptions: SendMailOptions = {
      from: `"Soroosh" <${SMTPOptions.auth.user}>`, // sender address
      to: to, // list of receivers
      subject: subject, // Subject line
    };
    if (text) {
      mailOptions.text = text;
    }
    if (html) {
      mailOptions.html = html;
    }
    await transporter.verify();
    return transporter.sendMail(mailOptions);
  }

  async sendConfirmMail({
    to,
    code,
    fullName,
    link,
  }: {
    to: string;
    code: string;
    fullName: string;
    link: string;
  }): Promise<void> {
    const emailText = `Dear ${fullName}, 
    your confirmation code is: ${code}
    You can also verify your email with this link: ${link}`;
    const template = compile(
      fs.readFileSync(
        path.join(process.cwd(), './views/confirm-email.hbs'),
        'utf8',
      ),
    );
    const emailHtml = template({
      code,
      fullName,
      link,
      brand: 'Soroosh',
    });
    await this.sendMail({
      to,
      subject: 'Confirm Email ✔',
      text: emailText,
      html: emailHtml,
    });
  }

  async sendResetPasswordMail({
    to,
    fullName,
    link,
  }: {
    to: string;
    fullName: string;
    link: string;
  }): Promise<void> {
    const emailText = `Dear ${fullName}, 
    You forgot your password! its fine! just use this link for reset your password: ${link}`;
    const template = compile(
      fs.readFileSync(
        path.join(process.cwd(), './views/reset-password.hbs'),
        'utf8',
      ),
    );
    const emailHtml = template({
      fullName,
      link,
      brand: 'Soroosh',
    });
    await this.sendMail({
      to,
      subject: 'Reset Password !',
      text: emailText,
      html: emailHtml,
    });
  }
}
