import { Injectable } from '@nestjs/common';
import * as querystring from 'querystring';
import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';

@Injectable()
export class ApiService {
  private handler(error: any) {
    if (error.response && error.response.data) {
      return error.response.data;
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      // console.error(error.response.data);
      // console.error(error.response.status);
      // console.error(error.response.headers);
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      // http.ClientRequest in node.js
      // console.error(error.request);
    } else {
      // Something happened in setting up the request that triggered an Error
      // console.error('Error', error.message);
    }
    return error;
    // console.error(error.config);
  }
  async get<T>({
    endpoint,
    data,
  }: {
    endpoint: string;
    data?: { [key: string]: any };
  }): Promise<T> {
    if (data) {
      endpoint += '?' + querystring.stringify(data);
    }
    let response: AxiosResponse<T>;
    try {
      response = await axios.get<T>(endpoint);
    } catch (error) {
      throw this.handler(error);
    }
    if (response.status === 200) {
      return response.data;
    } else {
      throw this.handler(response);
    }
  }
  async post<T>({
    endpoint,
    data,
    options,
  }: {
    endpoint: string;
    data: { [key: string]: any };
    options?: AxiosRequestConfig;
  }): Promise<T> {
    let response: AxiosResponse<T>;
    try {
      response = await axios.post(endpoint, data, options);
    } catch (error) {
      throw this.handler(error);
    }
    if (response.status === 200) {
      return response.data;
    } else {
      throw this.handler(response);
    }
  }
}
