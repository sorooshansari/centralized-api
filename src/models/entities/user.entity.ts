/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  Column,
  CreateDateColumn, Entity,
  JoinTable, ManyToMany
} from 'typeorm';
import { UserType } from '../enums/user-type.enum';
import { BaseEntity } from './base.entity';
import { Role } from './role.entity';

@Entity({ name: 'user' })
export class User extends BaseEntity {
  @Column({ type: 'varchar', length: 300 })
  name: string;

  @Column({ type: 'varchar', length: 300 })
  email: string;

  @Column({ type: 'boolean', default: false })
  isEmailVerified: boolean;

  @Column({ type: 'varchar', length: 300 })
  phone: string;

  @Column({ type: 'boolean', default: false })
  isPhoneVerified: boolean;

  @Column({ type: 'varchar', length: 300 })
  password: string;

  @Column({ type: 'varchar', length: 300, nullable: true })
  emailToken?: string;

  @CreateDateColumn({ type: 'timestamptz', nullable: true })
  emailTokenExpiration?: Date;

  @Column({ type: 'varchar', length: 300, nullable: true })
  phoneToken?: string;

  @CreateDateColumn({ type: 'timestamptz', nullable: true })
  phoneTokenExpiration?: Date;

  @Column({ type: 'varchar', length: 300, nullable: true })
  changePassToken?: string;

  @CreateDateColumn({ type: 'timestamptz', nullable: true })
  changePassTokenExpiration?: Date;

  @Column({ type: 'varchar', length: 300, nullable: true })
  tempEmail?: string;

  @Column({ type: 'varchar', length: 300, nullable: true })
  changeEmailToken?: string;

  @CreateDateColumn({ type: 'timestamptz', nullable: true })
  changeEmailTokenExpiration?: Date;

  @CreateDateColumn({ type: 'int', default: UserType.USER })
  type: UserType;

  @ManyToMany(_type => Role)
  @JoinTable()
  roles: Role[];
}
