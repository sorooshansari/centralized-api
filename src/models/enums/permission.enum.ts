export enum Permission {
  'ROLE.VIEW' = 1,
  'ROLE.CREATE',
  'ROLE.UPDATE',
  'ROLE.DELETE',
  'USER.VIEW',
  'USER.CREATE',
  'USER.UPDATE',
  'USER.DELETE',
}
