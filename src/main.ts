import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as bodyParser from 'body-parser';
import { ErrorsInterceptor } from './common/interceptors/errors.interceptor';
import { ValidationPipe, Logger } from '@nestjs/common';
import { LoggingInterceptor } from './common/interceptors/logging.interceptor';
declare const module: any;

async function bootstrap() {
  const logger = new Logger();
  const app = await NestFactory.create(AppModule, {
    logger: logger,
    cors: true,
  });
  app.use(bodyParser.json());
  app.useGlobalPipes(new ValidationPipe({ transform: true }));
  app.useGlobalInterceptors(
    new ErrorsInterceptor(logger),
    new LoggingInterceptor(logger)
  );

  const port: number = process.env.PORT ? parseInt(process.env.PORT, 10) : 4242;
  await app.listen(port);
  const url = await app.getUrl();
  if (url.includes('[::1]')) {
    console.log(`App is running on: http://localhost:${port}`);
  } else {
    console.log(`Application is running on: ${url}`);
  }

  if (module.hot) {
    module.hot.accept();
    module.hot.dispose(() => app.close());
  }
}
bootstrap();
