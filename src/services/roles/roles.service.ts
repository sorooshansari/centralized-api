import { Injectable } from '@nestjs/common';
import { Role } from '../../models/entities/role.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { BaseService } from '../base.service';
import { Repository } from 'typeorm';

@Injectable()
export class RolesService extends BaseService<Role> {
  constructor(
    @InjectRepository(Role)
    private rolesRepository: Repository<Role>,
  ) {
    super(rolesRepository);
  }
}
