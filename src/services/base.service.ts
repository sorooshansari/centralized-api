import {
  Repository,
  DeepPartial,
  FindConditions,
  UpdateResult,
  ObjectLiteral,
} from 'typeorm';
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity';

export class BaseService<T> {
  constructor(private entityRepository: Repository<T>) {}

  save(entity: DeepPartial<T>): Promise<T> {
    return this.entityRepository.save(entity);
  }

  updateOne(
    id: string,
    entity: QueryDeepPartialEntity<T>,
  ): Promise<UpdateResult> {
    return this.entityRepository.update(id, entity);
  }

  findOneWithRelations(id: string, relations: string[]): Promise<T> {
    return this.entityRepository.findOne(id, { relations });
  }

  find(
    skip: number,
    limit: number,
    where: string | FindConditions<T> | ObjectLiteral | FindConditions<T>[],
  ): Promise<[T[], number]> {
    return this.entityRepository.findAndCount({
      where,
      skip,
      take: limit,
      order: { id: 'DESC' } as any,
    });
  }

  findOne(id: string): Promise<T> {
    return this.entityRepository.findOne(id);
  }

  findBy(conditions: FindConditions<T>): Promise<T[]> {
    return this.entityRepository.find(conditions);
  }

  findOneBy(conditions: FindConditions<T>): Promise<T> {
    return this.entityRepository.findOne(conditions);
  }

  async remove(ids: string[]): Promise<void> {
    await this.entityRepository.delete(ids);
  }
}
