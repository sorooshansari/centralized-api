/* eslint-disable @typescript-eslint/no-unused-vars */
import { IsUUID } from 'class-validator';

export class RemoveRoleDto {
  @IsUUID('4', { each: true })
  ids: string[];
}
