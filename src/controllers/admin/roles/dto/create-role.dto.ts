/* eslint-disable @typescript-eslint/no-unused-vars */
import { IsString, MaxLength, IsNumber } from 'class-validator';

export class CreateRoleDto {
  @IsString()
  @MaxLength(30)
  name: string;

  @IsString()
  @MaxLength(300)
  description: string;

  @IsNumber({}, { each: true })
  permissions: number[];
}
