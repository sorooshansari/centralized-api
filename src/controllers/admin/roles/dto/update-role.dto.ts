/* eslint-disable @typescript-eslint/no-unused-vars */
import { IsNumber, IsUUID, IsString, MaxLength, IsArray } from 'class-validator';

export class UpdateRoleDto {
  @IsUUID()
  id: string;

  @IsString()
  @MaxLength(30)
  name: string;

  @IsString()
  @MaxLength(300)
  description: string;

  @IsArray()
  @IsNumber({}, { each: true })
  permissions: number[];
}
