import {
  Controller,
  Get,
  UseGuards,
  Query,
  Param,
  Post,
  Body,
  Patch,
  Delete,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { RolesService } from '../../../services/roles/roles.service';
import { Role } from '../../../models/entities/role.entity';
import { SetPermission } from '../../../common/decorators/set-permission.decorator';
import { Permission } from '../../../models/enums/permission.enum';
import { PermissionGuard } from '../../../auth/permission.guard';
import { CreateRoleDto } from './dto/create-role.dto';
import { UpdateRoleDto } from './dto/update-role.dto';
import { User } from '../../../models/entities/user.entity';
import { GetUser } from '../../../common/decorators/user.decorator';
import { Raw } from 'typeorm';
import { RemoveRoleDto } from './dto/remove-role.dto';

@Controller('admin/roles')
export class RolesController {
  constructor(private rolesService: RolesService) {}

  @Get()
  @SetPermission(Permission['ROLE.VIEW'])
  @UseGuards(AuthGuard('jwt'), PermissionGuard)
  getAll(
    @Query('skip') skip: number,
    @Query('limit') limit: number,
    @Query('query') query: string,
  ): Promise<[Role[], number]> {
    const where =
      query && query.length
        ? [
            {
              name: Raw(
                alias => `LOWER(${alias}) Like '%${query.toLowerCase()}%'`,
              ),
            },
          ]
        : null;
    return this.rolesService.find(skip, limit, where);
  }

  @Get(':id')
  @SetPermission(Permission['ROLE.VIEW'])
  @UseGuards(AuthGuard('jwt'), PermissionGuard)
  getOne(@Param('id') id: string): Promise<Role> {
    return this.rolesService.findOne(id);
  }

  @Post()
  @SetPermission(Permission['ROLE.CREATE'])
  @UseGuards(AuthGuard('jwt'), PermissionGuard)
  create(@GetUser() user: User, @Body() dto: CreateRoleDto): Promise<Role> {
    return this.rolesService.save({
      name: dto.name,
      description: dto.description,
      permissions: dto.permissions.join(','),
      createdBy: user.email,
      lastChangedBy: user.email,
    });
  }

  @Patch()
  @SetPermission(Permission['ROLE.UPDATE'])
  @UseGuards(AuthGuard('jwt'), PermissionGuard)
  async update(@Body() dto: UpdateRoleDto): Promise<Role> {
    await this.rolesService.updateOne(dto.id, {
      name: dto.name,
      description: dto.description,
      permissions: dto.permissions.join(','),
    });
    return this.rolesService.findOne(dto.id);
  }

  @Delete()
  @SetPermission(Permission['ROLE.DELETE'])
  @UseGuards(AuthGuard('jwt'), PermissionGuard)
  delete(@Body() dto: RemoveRoleDto): Promise<void> {
    return this.rolesService.remove(dto.ids);
  }
}
