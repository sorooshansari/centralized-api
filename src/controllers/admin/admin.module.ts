import { Module } from '@nestjs/common';
import { RolesController } from './roles/roles.controller';
import { RolesServiceModule } from '../../services/roles/roles.service.module';
import { UsersServiceModule } from '../../services/users/users.service.module';

@Module({
  imports: [RolesServiceModule, UsersServiceModule],
  controllers: [RolesController],
})
export class AdminModule {}
