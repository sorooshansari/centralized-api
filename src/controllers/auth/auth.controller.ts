import {
  Body,
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Logger,
  Post,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from '../../auth/auth.service';
import { IAuthUser } from '../../auth/authUser.model';
import { AuthUser } from '../../common/decorators/auth-user.decorator';
import { configService } from '../../config/config.service';
import {
  EMAIL_EXISTS,
  PASSWORD_NOT_VALID,
  TOKEN_EXPIRED,
  TOKEN_NOT_VALID,
  USER_NOT_FOUND,
} from '../../constants/errors.constant';
import { User } from '../../models/entities/user.entity';
import { Permission } from '../../models/enums/permission.enum';
import { EmailService } from '../../providers/email/email.service';
import { Helpers } from '../../providers/helpers/helpers';
import { PasswordCryptographerService } from '../../providers/password-cryptographer/password-cryptographer';
import { UsersService } from '../../services/users/users.service';
import { ChangeEmailDto } from './dto/change-email.dto';
import { ChangePasswordDto } from './dto/change-password.dto';
import { CheckEmailDto } from './dto/check-email.dto';
import { ForgotPasswordDto } from './dto/forgot-password.dto';
import { LoginDto } from './dto/login.dto';
import { RegisterDto } from './dto/register.dto';
import { ResendVerifyEmailDto } from './dto/resend-verify-email.dto';
import { ResetPasswordDto } from './dto/reset-password.dto';
import { UpdateProfileDto } from './dto/update-profile.dto';
import { VerifyChangeEmailDto } from './dto/verify-change-email.dto';
import { VerifyEmailDto } from './dto/verify-email.dto';

@Controller('auth')
export class AuthController {
  constructor(
    private usersService: UsersService,
    private encryptionService: PasswordCryptographerService,
    private authService: AuthService,
    private helpers: Helpers,
    private emailService: EmailService,
  ) {}

  @Get('permissions')
  @UseGuards(AuthGuard('jwt'))
  getPermissions(): { name: string; value: number }[] {
    return Object.keys(Permission)
      .filter(value => isNaN(Number(value)) === true)
      .map(key => ({
        name: key,
        value: Permission[key],
      }));
  }

  @Get('self')
  @UseGuards(AuthGuard('jwt'))
  self(@AuthUser() authUser: IAuthUser): Promise<User> {
    return this.usersService.findOne(authUser.id);
  }

  @Post('login')
  async login(@Body() dto: LoginDto): Promise<{ user: User; token: string }> {
    let user = await this.usersService.findOneBy({
      email: dto.email,
    });
    if (
      !user ||
      !(await this.encryptionService.doCompare(dto.password, user.password))
    ) {
      throw new HttpException(USER_NOT_FOUND, HttpStatus.I_AM_A_TEAPOT);
    }
    if (!user.isEmailVerified) {
      let emailToken = user.emailToken
        ? this.encryptionService.decrypt(user.emailToken)
        : null;
      if (
        !user.emailTokenExpiration ||
        user.emailTokenExpiration.getTime() < new Date().getTime()
      ) {
        emailToken = this.helpers.generateRandomNumber();
        user.emailToken = this.encryptionService.encrypt(emailToken);
        user.emailTokenExpiration = new Date(
          new Date().getTime() +
            configService.getTokenValidationTime() * 60 * 60 * 1000,
        );
      }
      user = await this.usersService.save(user);

      try {
        await this.emailService.sendConfirmMail({
          to: user.email,
          code: emailToken,
          fullName: user.name,
          link: `${configService.getFrontendAddress()}/auth/verify-email?email=${
            user.email
          }&code=${emailToken}`,
        });
      } catch (error) {
        Logger.error(
          error,
          `${AuthController.name}/${this.register.name}`,
          'sending confirmation email failed',
        );
        throw error;
      }
      // throw new HttpException(EMAIL_NOT_VERIFIED, HttpStatus.I_AM_A_TEAPOT);
    }

    const token = await this.authService.createToken({
      email: user.email,
      id: user.id,
      type: user.type,
    });

    return { user, token };
  }

  @Post('register')
  async register(@Body() dto: RegisterDto): Promise<{ user: User }> {
    let user = await this.usersService.findOneBy({
      email: dto.email,
    });
    if (!user) {
      user = new User();
      user.email = dto.email;
    }
    if (user.isEmailVerified) {
      throw new HttpException(EMAIL_EXISTS, HttpStatus.I_AM_A_TEAPOT);
    }
    user.name = dto.name;
    user.password = await this.encryptionService.doHash(dto.password);
    user.phone = dto.phone;
    let emailToken = user.emailToken
      ? this.encryptionService.decrypt(user.emailToken)
      : null;
    if (
      !user.emailTokenExpiration ||
      user.emailTokenExpiration.getTime() < new Date().getTime()
    ) {
      emailToken = this.helpers.generateRandomNumber();
      user.emailToken = this.encryptionService.encrypt(emailToken);
      user.emailTokenExpiration = new Date(
        new Date().getTime() +
          configService.getTokenValidationTime() * 60 * 60 * 1000,
      );
    }
    user = await this.usersService.save(user);

    try {
      await this.emailService.sendConfirmMail({
        to: user.email,
        code: emailToken,
        fullName: user.name,
        link: `${configService.getFrontendAddress()}/auth/verify-email?email=${
          user.email
        }&code=${emailToken}`,
      });
    } catch (error) {
      Logger.error(
        error,
        `${AuthController.name}/${this.register.name}`,
        'sending confirmation email failed',
      );
      throw error;
    }
    return { user };
  }

  @Post('resent-verify-email')
  async resentVerifyEmail(
    @Body() dto: ResendVerifyEmailDto,
  ): Promise<{ status: boolean }> {
    const user = await this.usersService.findOneBy({
      email: dto.email,
    });
    if (!user) {
      throw new HttpException(USER_NOT_FOUND, HttpStatus.I_AM_A_TEAPOT);
    }
    if (user.isEmailVerified) {
      throw new HttpException(EMAIL_EXISTS, HttpStatus.I_AM_A_TEAPOT);
    }

    let emailToken = user.emailToken
      ? this.encryptionService.decrypt(user.emailToken)
      : null;
    if (
      !user.emailTokenExpiration ||
      user.emailTokenExpiration.getTime() < new Date().getTime()
    ) {
      emailToken = this.helpers.generateRandomNumber();
      const newEmailToken = this.encryptionService.encrypt(emailToken);
      const newEmailTokenExpiration = new Date(
        new Date().getTime() +
          configService.getTokenValidationTime() * 60 * 60 * 1000,
      );

      await this.usersService.updateOne(user.id, {
        changePassToken: newEmailToken,
        changePassTokenExpiration: newEmailTokenExpiration,
      });
    }

    try {
      await this.emailService.sendConfirmMail({
        to: user.email,
        code: emailToken,
        fullName: user.name,
        link: `${configService.getFrontendAddress()}/auth/verify-email?email=${
          user.email
        }&code=${emailToken}`,
      });
    } catch (error) {
      Logger.error(
        error,
        `${AuthController.name}/${this.register.name}`,
        'sending confirmation email failed',
      );
      throw error;
    }
    return { status: true };
  }

  @Post('check-email')
  async checkEmail(@Body() dto: CheckEmailDto): Promise<{ status: boolean }> {
    const user = await this.usersService.findOneBy({
      email: dto.email,
    });

    return {
      status: user && user.isEmailVerified,
    };
  }

  @Post('verify-email')
  async verifyEmail(
    @Body() dto: VerifyEmailDto,
  ): Promise<{ user: User; token: string }> {
    let user = await this.usersService.findOneBy({
      email: dto.email,
    });
    if (!user) {
      throw new HttpException(USER_NOT_FOUND, HttpStatus.I_AM_A_TEAPOT);
    }
    if (user.isEmailVerified) {
      throw new HttpException(EMAIL_EXISTS, HttpStatus.I_AM_A_TEAPOT);
    }
    if (
      !user.emailTokenExpiration ||
      user.emailTokenExpiration.getTime() < new Date().getTime()
    ) {
      throw new HttpException(TOKEN_EXPIRED, HttpStatus.I_AM_A_TEAPOT);
    }

    const emailToken = user.emailToken
      ? this.encryptionService.decrypt(user.emailToken)
      : null;

    if (emailToken !== dto.code) {
      throw new HttpException(TOKEN_NOT_VALID, HttpStatus.I_AM_A_TEAPOT);
    }

    user.isEmailVerified = true;
    const token = await this.authService.createToken({
      email: user.email,
      id: user.id,
      type: user.type,
    });
    user = await this.usersService.save(user);
    return { user, token };
  }

  @Post('forgot-password')
  async forgotPassword(
    @Body() dto: ForgotPasswordDto,
  ): Promise<{ status: boolean }> {
    const user = await this.usersService.findOneBy({
      email: dto.email,
    });

    if (!user) {
      throw new HttpException(USER_NOT_FOUND, HttpStatus.I_AM_A_TEAPOT);
    }

    let changePassToken = user.changePassToken
      ? this.encryptionService.decrypt(user.changePassToken)
      : null;
    if (
      !user.changePassTokenExpiration ||
      user.changePassTokenExpiration.getTime() < new Date().getTime()
    ) {
      changePassToken = this.helpers.generateRandomNumber();
      const newChangePassToken = this.encryptionService.encrypt(
        changePassToken,
      );
      const newChangePassTokenExpiration = new Date(
        new Date().getTime() +
          configService.getTokenValidationTime() * 60 * 60 * 1000,
      );
      await this.usersService.updateOne(user.id, {
        changePassToken: newChangePassToken,
        changePassTokenExpiration: newChangePassTokenExpiration,
      });
    }

    try {
      await this.emailService.sendResetPasswordMail({
        to: user.email,
        fullName: user.name,
        link: `${configService.getFrontendAddress()}/auth/reset-password?email=${
          user.email
        }&code=${changePassToken}`,
      });
    } catch (error) {
      Logger.error(
        error,
        `${AuthController.name}/${this.register.name}`,
        'sending confirmation email failed',
      );
      throw error;
    }
    return { status: true };
  }

  @Post('reset-password')
  async resetPassword(
    @Body() dto: ResetPasswordDto,
  ): Promise<{ status: boolean }> {
    const user = await this.usersService.findOneBy({
      email: dto.email,
    });

    if (!user) {
      throw new HttpException(USER_NOT_FOUND, HttpStatus.I_AM_A_TEAPOT);
    }

    if (
      !user.changePassTokenExpiration ||
      user.changePassTokenExpiration.getTime() < new Date().getTime()
    ) {
      throw new HttpException(TOKEN_EXPIRED, HttpStatus.I_AM_A_TEAPOT);
    }

    const changePassToken = user.changePassToken
      ? this.encryptionService.decrypt(user.changePassToken)
      : null;

    if (changePassToken !== dto.code) {
      throw new HttpException(TOKEN_NOT_VALID, HttpStatus.I_AM_A_TEAPOT);
    }
    const newPass = await this.encryptionService.doHash(dto.password);

    try {
      await this.usersService.updateOne(user.id, { password: newPass });
    } catch (error) {
      Logger.error(
        error,
        `${AuthController.name}/${this.resetPassword.name}`,
        'reset password failed',
      );
      throw error;
    }

    return { status: true };
  }

  @Post('update-profile')
  @UseGuards(AuthGuard('jwt'))
  async updateProfile(
    @AuthUser() authUser: IAuthUser,
    @Body() dto: UpdateProfileDto,
  ): Promise<{ user: User }> {
    let user = await this.usersService.findOne(authUser.id);

    if (!user) {
      throw new HttpException(USER_NOT_FOUND, HttpStatus.I_AM_A_TEAPOT);
    }

    user.name = dto.name;
    user.phone = dto.phone;

    try {
      user = await this.usersService.save(user);
    } catch (error) {
      Logger.error(
        error,
        `${AuthController.name}/${this.resetPassword.name}`,
        'update user failed',
      );
      throw error;
    }

    return { user: user };
  }

  @Post('change-password')
  @UseGuards(AuthGuard('jwt'))
  async changePassword(
    @AuthUser() authUser: IAuthUser,
    @Body() dto: ChangePasswordDto,
  ): Promise<{ status: boolean }> {
    const user = await this.usersService.findOne(authUser.id);

    if (!user) {
      throw new HttpException(USER_NOT_FOUND, HttpStatus.I_AM_A_TEAPOT);
    }

    if (
      !(await this.encryptionService.doCompare(
        dto.currentPassword,
        user.password,
      ))
    ) {
      throw new HttpException(PASSWORD_NOT_VALID, HttpStatus.I_AM_A_TEAPOT);
    }

    const newPass = await this.encryptionService.doHash(dto.newPassword);

    try {
      await this.usersService.updateOne(user.id, { password: newPass });
    } catch (error) {
      Logger.error(
        error,
        `${AuthController.name}/${this.resetPassword.name}`,
        'change password failed',
      );
      throw error;
    }

    return { status: true };
  }

  @Post('change-email')
  @UseGuards(AuthGuard('jwt'))
  async changeEmail(
    @AuthUser() authUser: IAuthUser,
    @Body() dto: ChangeEmailDto,
  ): Promise<{ status: boolean }> {
    const user = await this.usersService.findOne(authUser.id);
    if (!user) {
      throw new HttpException(USER_NOT_FOUND, HttpStatus.I_AM_A_TEAPOT);
    }
    let changeEmailToken = user.changeEmailToken
      ? this.encryptionService.decrypt(user.changeEmailToken)
      : null;
    if (
      !user.changeEmailTokenExpiration ||
      user.changeEmailTokenExpiration.getTime() < new Date().getTime()
    ) {
      changeEmailToken = this.helpers.generateRandomNumber();
      const newChangeEmailToken = this.encryptionService.encrypt(
        changeEmailToken,
      );
      const newChangeEmailTokenExpiration = new Date(
        new Date().getTime() +
          configService.getTokenValidationTime() * 60 * 60 * 1000,
      );
      await this.usersService.updateOne(user.id, {
        changeEmailToken: newChangeEmailToken,
        changeEmailTokenExpiration: newChangeEmailTokenExpiration,
      });
    }
    await this.usersService.updateOne(user.id, {
      tempEmail: dto.email,
    });

    try {
      await this.emailService.sendConfirmMail({
        to: dto.email,
        code: changeEmailToken,
        fullName: user.name,
        link: `${configService.getFrontendAddress()}/auth/change-email-verification?&code=${changeEmailToken}`,
      });
    } catch (error) {
      Logger.error(
        error,
        `${AuthController.name}/${this.register.name}`,
        'sending confirmation email changing failed',
      );
      throw error;
    }
    return { status: true };
  }

  @Post('verify-change-email')
  @UseGuards(AuthGuard('jwt'))
  async verifyChangeEmail(
    @AuthUser() authUser: IAuthUser,
    @Body() dto: VerifyChangeEmailDto,
  ): Promise<{ status: boolean }> {
    const user = await this.usersService.findOne(authUser.id);
    if (!user) {
      throw new HttpException(USER_NOT_FOUND, HttpStatus.I_AM_A_TEAPOT);
    }

    if (
      !user.changeEmailTokenExpiration ||
      user.changeEmailTokenExpiration.getTime() < new Date().getTime()
    ) {
      throw new HttpException(TOKEN_EXPIRED, HttpStatus.I_AM_A_TEAPOT);
    }

    const changeEmailToken = user.changeEmailToken
      ? this.encryptionService.decrypt(user.changeEmailToken)
      : null;

    if (changeEmailToken !== dto.code) {
      throw new HttpException(TOKEN_NOT_VALID, HttpStatus.I_AM_A_TEAPOT);
    }

    try {
      await this.usersService.updateOne(user.id, {
        email: user.tempEmail,
        tempEmail: null,
      });
    } catch (error) {
      Logger.error(
        error,
        `${AuthController.name}/${this.register.name}`,
        'changing email verification failed',
      );
      throw error;
    }

    return { status: true };
  }

  @Post('resent-verify-change-email')
  @UseGuards(AuthGuard('jwt'))
  async resentVerifyChangeEmail(
    @AuthUser() authUser: IAuthUser,
  ): Promise<{ status: boolean }> {
    const user = await this.usersService.findOne(authUser.id);
    if (!user) {
      throw new HttpException(USER_NOT_FOUND, HttpStatus.I_AM_A_TEAPOT);
    }

    let changeEmailToken = user.changeEmailToken
      ? this.encryptionService.decrypt(user.changeEmailToken)
      : null;
    if (
      !user.changeEmailTokenExpiration ||
      user.changeEmailTokenExpiration.getTime() < new Date().getTime()
    ) {
      changeEmailToken = this.helpers.generateRandomNumber();
      const newChangeEmailToken = this.encryptionService.encrypt(
        changeEmailToken,
      );
      const newChangeEmailTokenExpiration = new Date(
        new Date().getTime() +
          configService.getTokenValidationTime() * 60 * 60 * 1000,
      );
      await this.usersService.updateOne(user.id, {
        changeEmailToken: newChangeEmailToken,
        changeEmailTokenExpiration: newChangeEmailTokenExpiration,
      });
    }

    try {
      await this.emailService.sendConfirmMail({
        to: user.tempEmail,
        code: changeEmailToken,
        fullName: user.name,
        link: `${configService.getFrontendAddress()}/auth/change-email-verification?&code=${changeEmailToken}`,
      });
    } catch (error) {
      Logger.error(
        error,
        `${AuthController.name}/${this.register.name}`,
        'sending confirmation email changing failed',
      );
      throw error;
    }
    return { status: true };
  }
}
