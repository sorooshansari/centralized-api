/* eslint-disable @typescript-eslint/no-unused-vars */
import { IsEmail, IsString, Length, MaxLength } from 'class-validator';

export class VerifyEmailDto {
  @IsString()
  @MaxLength(5)
  code: string;

  @IsEmail()
  @Length(2, 30)
  email: string;
}
