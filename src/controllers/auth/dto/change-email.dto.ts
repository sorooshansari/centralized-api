/* eslint-disable @typescript-eslint/no-unused-vars */
import { IsEmail, Length } from 'class-validator';

export class ChangeEmailDto {
  @IsEmail()
  @Length(2, 30)
  email: string;
}
