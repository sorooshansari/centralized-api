/* eslint-disable @typescript-eslint/no-unused-vars */
import { IsString, Length, MaxLength } from 'class-validator';

export class UpdateProfileDto {
  @IsString()
  @MaxLength(30)
  name: string;

  @IsString()
  @Length(4, 20)
  phone: string;
}
