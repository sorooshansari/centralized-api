/* eslint-disable @typescript-eslint/no-unused-vars */
import { IsString, MaxLength } from 'class-validator';

export class VerifyChangeEmailDto {
  @IsString()
  @MaxLength(5)
  code: string;
}
