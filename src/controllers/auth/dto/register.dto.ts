/* eslint-disable @typescript-eslint/no-unused-vars */
import { IsEmail, IsString, Length, MaxLength } from 'class-validator';

export class RegisterDto {
  @IsString()
  @MaxLength(30)
  name: string;

  @IsEmail()
  @Length(2, 30)
  email: string;

  @IsString()
  @Length(4, 50)
  password: string;

  @IsString()
  @Length(4, 20)
  phone: string;
}
