/* eslint-disable @typescript-eslint/no-unused-vars */
import { IsString, Length } from 'class-validator';

export class ChangePasswordDto {
  @IsString()
  @Length(2, 30)
  currentPassword: string;

  @IsString()
  @Length(2, 30)
  newPassword: string;
}
