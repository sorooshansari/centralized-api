/* eslint-disable @typescript-eslint/no-unused-vars */
import { IsEmail, IsString, Length, MaxLength } from 'class-validator';

export class ResetPasswordDto {
  @IsEmail()
  @Length(2, 30)
  email: string;

  @IsString()
  @MaxLength(5)
  code: string;

  @IsString()
  @Length(2, 30)
  password: string;
}
