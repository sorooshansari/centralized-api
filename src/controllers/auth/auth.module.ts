import { Module } from '@nestjs/common';
import { UsersServiceModule } from '../../services/users/users.service.module';
import { PasswordCryptographerServiceModule } from '../../providers/password-cryptographer/password-cryptographer.module';
import { AuthServiceModule } from '../../auth/auth.module';
import { HelpersModule } from '../../providers/helpers/helpers.module';
import { EmailServiceModule } from '../../providers/email/email.module';
import { AuthController } from './auth.controller';

@Module({
  imports: [
    UsersServiceModule,
    PasswordCryptographerServiceModule,
    AuthServiceModule,
    HelpersModule,
    EmailServiceModule,
  ],
  controllers: [AuthController],
})
export class AuthModule {}
