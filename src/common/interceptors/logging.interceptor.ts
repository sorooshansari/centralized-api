import { NestInterceptor, ExecutionContext, Injectable, CallHandler, Logger } from '@nestjs/common';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable()
export class LoggingInterceptor implements NestInterceptor {
  constructor(
    private readonly log: Logger,
  ) { }
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    this.log.debug('Before...', `${context.getClass().name}/${context.getHandler().name}`);

    const now = Date.now();
    return next
      .handle()
      .pipe(
        tap(() => this.log.debug(`After... ${Date.now() - now}ms`, `${context.getClass().name}/${context.getHandler().name}`)),
      );
  }
}
// @Interceptor()
// export class LoggingInterceptor implements NestInterceptor {
//   intercept(dataOrRequest, context: ExecutionContext, stream$: Observable<any>): Observable<any> {
//     console.log('Before...');
//     const now = Date.now();

//     return stream$.do(
//       () => console.log(`After... ${Date.now() - now}ms`),
//     );
//   }
// }
