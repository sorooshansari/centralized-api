import { SetMetadata, CustomDecorator } from '@nestjs/common';
import { Permission } from '../../models/enums/permission.enum';

export const SetPermission = (
  permission: Permission,
): CustomDecorator<string> => SetMetadata('permission', permission);
