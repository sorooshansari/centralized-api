import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { IAuthUser } from '../../auth/authUser.model';

export const AuthUser = createParamDecorator(
  (data: unknown, ctx: ExecutionContext): IAuthUser =>
    ctx.switchToHttp().getRequest().user,
);
