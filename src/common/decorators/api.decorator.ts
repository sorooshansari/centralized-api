// import * as path from 'path';
export const apiPath = (apiVersion: number, urlPrefix: string): string => {
  return `/api/v${apiVersion}/${urlPrefix}`;
};
