import { TypeOrmModuleOptions } from '@nestjs/typeorm';

require('dotenv').config();

class ConfigService {
  public getTokenValidationTime() {
    return Number(process.env.TOKEN_VALIDATION_TIME) || 1;
  }

  public getPort() {
    return process.env.PORT;
  }

  public isProduction() {
    const mode = process.env.MODE;
    return mode != 'DEV';
  }

  public getFrontendAddress() {
    return process.env.FRONTEND_ADDRESS;
  }

  public getAuthSecretToken() {
    return process.env.AUTH_SECRET_TOKEN;
  }

  public getAuthIntegrityToken() {
    return process.env.AUTH_INTEGRITY_TOKEN;
  }

  public getEncryptionInfo() {
    return {
      password: process.env.ENCRYPTION_PASSWORD,
      algorithm: process.env.ENCRYPTION_ALGORITHM,
    };
  }

  public getSMTPOptions(): any {
    return {
      service: 'gmail',
      auth: {
        type: 'OAuth2',
        user: process.env.SMTP_USER,
        // pass: process.env.SMTP_PASS,
        clientId: process.env.SMTP_CLIENT_ID,
        clientSecret: process.env.SMTP_CLIENT_SECRET,
        refreshToken: process.env.SMTP_REFRESH_TOKEN,
      },
    };
  }
  public getDatabaseConfig() {
    return {
      host: process.env.POSTGRES_HOST,
      port: parseInt(process.env.POSTGRES_PORT),
      username: process.env.POSTGRES_USER,
      password: process.env.POSTGRES_PASSWORD,
      database: process.env.POSTGRES_DATABASE,
    };
  }

  public getTypeOrmConfig(): TypeOrmModuleOptions {
    return {
      type: 'postgres',

      host: process.env.POSTGRES_HOST,
      port: parseInt(process.env.POSTGRES_PORT),
      username: process.env.POSTGRES_USER,
      password: process.env.POSTGRES_PASSWORD,
      database: process.env.POSTGRES_DATABASE,

      entities: ['src/**/*.entity{.ts,.js}'],

      migrationsTableName: 'migration',

      migrations: ['src/migration/*{.ts,.js}'],

      cli: {
        migrationsDir: 'src/migration',
      },

      ssl: this.isProduction(),
    };
  }
}

const configService = new ConfigService();

export { configService };
