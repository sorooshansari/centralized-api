import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { configService } from './config/config.service';
import { AuthModule } from './controllers/auth/auth.module';
import { AdminModule } from './controllers/admin/admin.module';
import { Role } from './models/entities/role.entity';
import { User } from './models/entities/user.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: configService.getDatabaseConfig().host,
      port: configService.getDatabaseConfig().port,
      username: configService.getDatabaseConfig().username,
      password: configService.getDatabaseConfig().password,
      database: configService.getDatabaseConfig().database,
      entities: [Role, User],
      synchronize: false,
    }),
    AuthModule,
    AdminModule,
  ],
})
export class AppModule {}
